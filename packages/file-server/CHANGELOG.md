# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-file-server@0.1.1...editoria-file-server@0.1.2) (2020-03-03)




**Note:** Version bump only for package editoria-file-server

<a name="0.1.1"></a>
## 0.1.1 (2019-10-14)




**Note:** Version bump only for package editoria-file-server
