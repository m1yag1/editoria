# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pdf-job@0.0.6...editoria-pdf-job@0.0.7) (2020-03-03)




**Note:** Version bump only for package editoria-pdf-job

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pdf-job@0.0.5...editoria-pdf-job@0.0.6) (2019-12-03)




**Note:** Version bump only for package editoria-pdf-job

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pdf-job@0.0.4...editoria-pdf-job@0.0.5) (2019-11-05)


### Bug Fixes

* **jobs:** fix in pdf and other bugs ([7bef376](https://gitlab.coko.foundation/editoria/editoria/commit/7bef376))




<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pdf-job@0.0.3...editoria-pdf-job@0.0.4) (2019-11-01)


### Bug Fixes

* **jobpdf:** correctionin of path ([e8b8021](https://gitlab.coko.foundation/editoria/editoria/commit/e8b8021))




<a name="0.0.3"></a>
## [0.0.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pdf-job@0.0.2...editoria-pdf-job@0.0.3) (2019-10-29)


### Bug Fixes

* **jobpdf:** dockerfile fix ([622bca9](https://gitlab.coko.foundation/editoria/editoria/commit/622bca9))




<a name="0.0.2"></a>
## 0.0.2 (2019-10-29)




**Note:** Version bump only for package editoria-pdf-job
