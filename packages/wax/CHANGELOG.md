# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.11.8"></a>
## [0.11.8](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.7...pubsweet-component-wax@0.11.8) (2020-03-03)




**Note:** Version bump only for package pubsweet-component-wax

<a name="0.11.7"></a>
## [0.11.7](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.6...pubsweet-component-wax@0.11.7) (2020-02-12)




**Note:** Version bump only for package pubsweet-component-wax

<a name="0.11.6"></a>
## [0.11.6](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.5...pubsweet-component-wax@0.11.6) (2020-01-31)




**Note:** Version bump only for package pubsweet-component-wax

<a name="0.11.5"></a>
## [0.11.5](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.4...pubsweet-component-wax@0.11.5) (2019-12-17)




**Note:** Version bump only for package pubsweet-component-wax

<a name="0.11.4"></a>
## [0.11.4](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.3...pubsweet-component-wax@0.11.4) (2019-11-05)


### Bug Fixes

* **jobs:** fix in pdf and other bugs ([7bef376](https://gitlab.coko.foundation/editoria/editoria/commit/7bef376))




<a name="0.11.3"></a>
## [0.11.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.2...pubsweet-component-wax@0.11.3) (2019-10-30)


### Bug Fixes

* **wax:** new wax ([6c7a3e6](https://gitlab.coko.foundation/editoria/editoria/commit/6c7a3e6))




<a name="0.11.2"></a>
## [0.11.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.1...pubsweet-component-wax@0.11.2) (2019-10-25)


### Bug Fixes

* **wax:** escaping character ([b893c4f](https://gitlab.coko.foundation/editoria/editoria/commit/b893c4f))




<a name="0.11.1"></a>
## [0.11.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.11.0...pubsweet-component-wax@0.11.1) (2019-10-17)


### Bug Fixes

* **wax:** fix for the inline custom ([2a97df2](https://gitlab.coko.foundation/editoria/editoria/commit/2a97df2))




<a name="0.11.0"></a>
# [0.11.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.10.1...pubsweet-component-wax@0.11.0) (2019-10-14)


### Bug Fixes

* **customtags:** subscription create fix ([e2165a4](https://gitlab.coko.foundation/editoria/editoria/commit/e2165a4))
* **wax pubsweet:** scroller fix ([059f9c0](https://gitlab.coko.foundation/editoria/editoria/commit/059f9c0))


### Features

* **customTag:** add subscription for custom tag update ([af4c7fd](https://gitlab.coko.foundation/editoria/editoria/commit/af4c7fd))
* **export:** export overhaul ([c06ed43](https://gitlab.coko.foundation/editoria/editoria/commit/c06ed43))




<a name="0.10.1"></a>
## [0.10.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.10.0...pubsweet-component-wax@0.10.1) (2019-08-02)


### Bug Fixes

* **template manager:** various fixes ([f7e79d4](https://gitlab.coko.foundation/editoria/editoria/commit/f7e79d4))




<a name="0.10.0"></a>
# [0.10.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.9.1...pubsweet-component-wax@0.10.0) (2019-08-01)


### Bug Fixes

* **app:** fixes for the beta release ([2ee72a4](https://gitlab.coko.foundation/editoria/editoria/commit/2ee72a4))
* **applicationmanager:** set default componentType for missing wax config ([8f9932c](https://gitlab.coko.foundation/editoria/editoria/commit/8f9932c))
* **customtags:** connect to editoria ([5f165a5](https://gitlab.coko.foundation/editoria/editoria/commit/5f165a5))
* **header:** add header to editor ([44f3521](https://gitlab.coko.foundation/editoria/editoria/commit/44f3521))
* **headertest:** fix header for test ([5a607d7](https://gitlab.coko.foundation/editoria/editoria/commit/5a607d7))
* **navigation:** clear uneened code ([54e4085](https://gitlab.coko.foundation/editoria/editoria/commit/54e4085))
* **navigation:** fix navigation of editor link ([c569dc2](https://gitlab.coko.foundation/editoria/editoria/commit/c569dc2))
* **navigation:** same width to all div ([97d8754](https://gitlab.coko.foundation/editoria/editoria/commit/97d8754))
* **wax:** height fo wax ([0ce2bfd](https://gitlab.coko.foundation/editoria/editoria/commit/0ce2bfd))
* **waxPubsweet:** fix subscritpions ([ef11796](https://gitlab.coko.foundation/editoria/editoria/commit/ef11796))


### Features

* **header:** easier navigation between bookcomponents ([9e675af](https://gitlab.coko.foundation/editoria/editoria/commit/9e675af))
* **tags:** add resolvers, graphql schema ([88bb4ca](https://gitlab.coko.foundation/editoria/editoria/commit/88bb4ca))




<a name="0.9.1"></a>
## [0.9.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.9.0...pubsweet-component-wax@0.9.1) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-wax

<a name="0.9.0"></a>
# [0.9.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.8.4...pubsweet-component-wax@0.9.0) (2019-05-22)


### Features

* upgraded substance ([466f92a](https://gitlab.coko.foundation/editoria/editoria/commit/466f92a))




<a name="0.8.4"></a>
## [0.8.4](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.8.3...pubsweet-component-wax@0.8.4) (2019-05-17)


### Bug Fixes

* restriction for file extension for Linux, handling of ink error ([4fb0cc5](https://gitlab.coko.foundation/editoria/editoria/commit/4fb0cc5))




<a name="0.8.3"></a>
## [0.8.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.8.2...pubsweet-component-wax@0.8.3) (2019-04-24)




**Note:** Version bump only for package pubsweet-component-wax

<a name="0.8.2"></a>
## [0.8.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.8.1...pubsweet-component-wax@0.8.2) (2019-04-24)


### Bug Fixes

* **trackchange:** get updates of the workflow and trackchange ([c0b0b3b](https://gitlab.coko.foundation/editoria/editoria/commit/c0b0b3b))
* more UI fixes ([78e14da](https://gitlab.coko.foundation/editoria/editoria/commit/78e14da))
* UI fixes ([92841d3](https://gitlab.coko.foundation/editoria/editoria/commit/92841d3))




<a name="0.8.1"></a>
## [0.8.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.8.0...pubsweet-component-wax@0.8.1) (2019-04-15)


### Bug Fixes

* wax export fix, invalid url fixes ([c42a96f](https://gitlab.coko.foundation/editoria/editoria/commit/c42a96f))




<a name="0.8.0"></a>
# [0.8.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.6.16...pubsweet-component-wax@0.8.0) (2019-04-12)


### Bug Fixes

* **authsome:** fix subscription for authosme updates ([1ce1fd9](https://gitlab.coko.foundation/editoria/editoria/commit/1ce1fd9))
* **navigation:** fix navigation back to book ([549e50b](https://gitlab.coko.foundation/editoria/editoria/commit/549e50b))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria/commit/be4d87b))
* **wax:** add user color ([c314de5](https://gitlab.coko.foundation/editoria/editoria/commit/c314de5))


### Features

* **authsome:** move authorize to backend ([4e00def](https://gitlab.coko.foundation/editoria/editoria/commit/4e00def))
* **wax:** add user to wax props ([b4e151e](https://gitlab.coko.foundation/editoria/editoria/commit/b4e151e))
* **wax:** upgrade wax editor ([34639a4](https://gitlab.coko.foundation/editoria/editoria/commit/34639a4))




<a name="0.7.0"></a>
# [0.7.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.6.16...pubsweet-component-wax@0.7.0) (2019-04-12)


### Bug Fixes

* **authsome:** fix subscription for authosme updates ([1ce1fd9](https://gitlab.coko.foundation/editoria/editoria/commit/1ce1fd9))
* **navigation:** fix navigation back to book ([549e50b](https://gitlab.coko.foundation/editoria/editoria/commit/549e50b))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria/commit/be4d87b))
* **wax:** add user color ([c314de5](https://gitlab.coko.foundation/editoria/editoria/commit/c314de5))


### Features

<<<<<<< HEAD
* upgraded substance ([466f92a](https://gitlab.coko.foundation/editoria/editoria/commit/466f92a))
=======
* **authsome:** move authorize to backend ([4e00def](https://gitlab.coko.foundation/editoria/editoria/commit/4e00def))
* **wax:** add user to wax props ([b4e151e](https://gitlab.coko.foundation/editoria/editoria/commit/b4e151e))
* **wax:** upgrade wax editor ([34639a4](https://gitlab.coko.foundation/editoria/editoria/commit/34639a4))
>>>>>>> signup-fix




<a name="0.6.16"></a>
## [0.6.16](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.6.15...pubsweet-component-wax@0.6.16) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-wax
