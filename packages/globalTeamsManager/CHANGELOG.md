# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.9"></a>
## [0.2.9](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.8...pubsweet-component-editoria-global-teams@0.2.9) (2020-03-03)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.8"></a>
## [0.2.8](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.7...pubsweet-component-editoria-global-teams@0.2.8) (2020-02-12)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.7"></a>
## [0.2.7](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.6...pubsweet-component-editoria-global-teams@0.2.7) (2020-01-31)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.6"></a>
## [0.2.6](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.5...pubsweet-component-editoria-global-teams@0.2.6) (2019-11-05)


### Bug Fixes

* **jobs:** fix in pdf and other bugs ([7bef376](https://gitlab.coko.foundation/editoria/editoria/commit/7bef376))




<a name="0.2.5"></a>
## [0.2.5](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.4...pubsweet-component-editoria-global-teams@0.2.5) (2019-10-14)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.3...pubsweet-component-editoria-global-teams@0.2.4) (2019-08-01)


### Bug Fixes

* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria/commit/8868af9))




<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.2...pubsweet-component-editoria-global-teams@0.2.3) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.1...pubsweet-component-editoria-global-teams@0.2.2) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.0...pubsweet-component-editoria-global-teams@0.2.1) (2019-04-24)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.1.2...pubsweet-component-editoria-global-teams@0.2.0) (2019-04-12)


### Bug Fixes

* **authsome:** add missing file ([5847772](https://gitlab.coko.foundation/editoria/editoria/commit/5847772))
* **globalteam:** migrate new tables for the globalteam page ([d8d0d37](https://gitlab.coko.foundation/editoria/editoria/commit/d8d0d37))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria/commit/be4d87b))


### Features

* **authsome:** add subscriptions to get updates ([c042093](https://gitlab.coko.foundation/editoria/editoria/commit/c042093))
* **globalteam:** migrate globalTeams to graphql ([2c6f870](https://gitlab.coko.foundation/editoria/editoria/commit/2c6f870))
* **teams:** migrate team to graphql ([cc161e9](https://gitlab.coko.foundation/editoria/editoria/commit/cc161e9))




<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.1.1...pubsweet-component-editoria-global-teams@0.1.2) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams
