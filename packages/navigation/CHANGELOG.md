# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.3.3"></a>
## [0.3.3](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.3.2...pubsweet-component-editoria-navigation@0.3.3) (2020-03-03)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.3.1...pubsweet-component-editoria-navigation@0.3.2) (2020-02-12)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.3.0...pubsweet-component-editoria-navigation@0.3.1) (2020-01-31)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.2.0...pubsweet-component-editoria-navigation@0.3.0) (2019-10-14)


### Features

* **profile:** new user page profile ([6e0fc62](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/6e0fc62))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.5...pubsweet-component-editoria-navigation@0.2.0) (2019-08-01)


### Bug Fixes

* **applicationmanager:** set default componentType for missing wax config ([8f9932c](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/8f9932c))
* **applicationparameters:** rename config to applicationmanager props everywhere ([47b3538](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/47b3538))
* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/8868af9))


### Features

* **applicationmanage:** create query ([02c84dd](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/02c84dd))
* **applicationmanager:** get live updates subscriptions ([632e471](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/632e471))
* **applicationparameters:** get config from db create graphql query ([3a34792](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/3a34792))




<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.4...pubsweet-component-editoria-navigation@0.1.5) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.3...pubsweet-component-editoria-navigation@0.1.4) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.2...pubsweet-component-editoria-navigation@0.1.3) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.1...pubsweet-component-editoria-navigation@0.1.2) (2019-04-24)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.1"></a>
## 0.1.1 (2019-04-12)


### Bug Fixes

* **navigation:** fix back to book on wax ([9482769](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/9482769))
* **navigation:** fix navigation back to book ([549e50b](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/549e50b))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/be4d87b))
* **wax:** add user color ([c314de5](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/c314de5))




# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.
