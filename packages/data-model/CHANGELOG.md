# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.3.3"></a>
## [0.3.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.3.2...editoria-data-model@0.3.3) (2020-03-03)




**Note:** Version bump only for package editoria-data-model

<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.3.1...editoria-data-model@0.3.2) (2020-02-12)




**Note:** Version bump only for package editoria-data-model

<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.3.0...editoria-data-model@0.3.1) (2019-10-15)


### Bug Fixes

* **exporter:** icml added and migration ([5f7efee](https://gitlab.coko.foundation/editoria/editoria/commit/5f7efee))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.2.0...editoria-data-model@0.3.0) (2019-10-14)


### Features

* **book builder:** init include in toc ([aafd09b](https://gitlab.coko.foundation/editoria/editoria/commit/aafd09b))
* **export:** export overhaul ([c06ed43](https://gitlab.coko.foundation/editoria/editoria/commit/c06ed43))
* **profile:** new user page profile ([6e0fc62](https://gitlab.coko.foundation/editoria/editoria/commit/6e0fc62))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.1.2...editoria-data-model@0.2.0) (2019-08-01)


### Bug Fixes

* **applicationmanager:** fix models retrieve confgi form db ([e441cb1](https://gitlab.coko.foundation/editoria/editoria/commit/e441cb1))
* **data model:** additions in template and file clean up ([ba558f1](https://gitlab.coko.foundation/editoria/editoria/commit/ba558f1))
* **datamodel:** target property added ([52cef1e](https://gitlab.coko.foundation/editoria/editoria/commit/52cef1e))
* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria/commit/8868af9))
* **test:** make them run after pubsweet upgrade ([1c50b43](https://gitlab.coko.foundation/editoria/editoria/commit/1c50b43))


### Features

* **applicationmanage:** create query ([02c84dd](https://gitlab.coko.foundation/editoria/editoria/commit/02c84dd))
* **data model:** support of files, files trnsaltions and templates ([283e762](https://gitlab.coko.foundation/editoria/editoria/commit/283e762))
* **data-model:** include in TOC, running headers support in DM ([e1ae06a](https://gitlab.coko.foundation/editoria/editoria/commit/e1ae06a))
* **dynamicomponenttype:** add graphql schema query mutations table ([4e4081d](https://gitlab.coko.foundation/editoria/editoria/commit/4e4081d))
* **tags:** add resolvers, graphql schema ([88bb4ca](https://gitlab.coko.foundation/editoria/editoria/commit/88bb4ca))




<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.1.1...editoria-data-model@0.1.2) (2019-05-28)




**Note:** Version bump only for package editoria-data-model

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.1.0...editoria-data-model@0.1.1) (2019-04-25)


### Bug Fixes

* publication date fix ([88cbfea](https://gitlab.coko.foundation/editoria/editoria/commit/88cbfea))




<a name="0.1.0"></a>
# 0.1.0 (2019-04-12)


### Bug Fixes

* **authsome:** lint problem ([7891594](https://gitlab.coko.foundation/editoria/editoria/commit/7891594))
* **data-model:** make tests run again ([b30372d](https://gitlab.coko.foundation/editoria/editoria/commit/b30372d))


### Features

* add first version of models with input validation ([b4adea3](https://gitlab.coko.foundation/editoria/editoria/commit/b4adea3))
* **app:** Given name and surname now supported in Signup ([745ab2e](https://gitlab.coko.foundation/editoria/editoria/commit/745ab2e)), closes [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **authorize:** update rules in certain events fo the user ([89ee01e](https://gitlab.coko.foundation/editoria/editoria/commit/89ee01e))
* **dashboard:** full name for user used in sorting ([64ec470](https://gitlab.coko.foundation/editoria/editoria/commit/64ec470))
* **data model:** add lock model ([a22d473](https://gitlab.coko.foundation/editoria/editoria/commit/a22d473))
* **data-model:** working book, division & component models ([3b2db01](https://gitlab.coko.foundation/editoria/editoria/commit/3b2db01))
* **dataloader:** create a data loader ([f5b12a8](https://gitlab.coko.foundation/editoria/editoria/commit/f5b12a8))
