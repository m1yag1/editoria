# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-pagedjs-viewer@0.1.2...pubsweet-component-pagedjs-viewer@0.1.3) (2020-03-03)




**Note:** Version bump only for package pubsweet-component-pagedjs-viewer

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-pagedjs-viewer@0.1.1...pubsweet-component-pagedjs-viewer@0.1.2) (2019-10-14)




**Note:** Version bump only for package pubsweet-component-pagedjs-viewer
