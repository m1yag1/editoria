# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/editoria/editoria-templates/compare/pubsweet-component-editoria-templates@0.2.2...pubsweet-component-editoria-templates@0.2.3) (2020-03-03)




**Note:** Version bump only for package pubsweet-component-editoria-templates

<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/editoria/editoria-templates/compare/pubsweet-component-editoria-templates@0.2.1...pubsweet-component-editoria-templates@0.2.2) (2020-02-12)




**Note:** Version bump only for package pubsweet-component-editoria-templates

<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/editoria/editoria-templates/compare/pubsweet-component-editoria-templates@0.2.0...pubsweet-component-editoria-templates@0.2.1) (2020-01-31)




**Note:** Version bump only for package pubsweet-component-editoria-templates

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria-templates/compare/pubsweet-component-editoria-templates@0.1.2...pubsweet-component-editoria-templates@0.2.0) (2019-10-14)


### Features

* **export:** export overhaul ([c06ed43](https://gitlab.coko.foundation/editoria/editoria-templates/commit/c06ed43))
* **template:** save pagedjs to new template ([75e48c0](https://gitlab.coko.foundation/editoria/editoria-templates/commit/75e48c0))




<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria-templates/compare/pubsweet-component-editoria-templates@0.1.1...pubsweet-component-editoria-templates@0.1.2) (2019-08-02)


### Bug Fixes

* **template manager:** various fixes ([f7e79d4](https://gitlab.coko.foundation/editoria/editoria-templates/commit/f7e79d4))




<a name="0.1.1"></a>
## 0.1.1 (2019-08-01)


### Bug Fixes

* **app:** fixes for the beta release ([2ee72a4](https://gitlab.coko.foundation/editoria/editoria-templates/commit/2ee72a4))
