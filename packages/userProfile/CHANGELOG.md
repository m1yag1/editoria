# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-user-profile@0.1.2...pubsweet-component-editoria-user-profile@0.1.3) (2020-03-03)




**Note:** Version bump only for package pubsweet-component-editoria-user-profile

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-user-profile@0.1.1...pubsweet-component-editoria-user-profile@0.1.2) (2020-02-12)




**Note:** Version bump only for package pubsweet-component-editoria-user-profile

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-user-profile@0.1.0...pubsweet-component-editoria-user-profile@0.1.1) (2020-01-31)




**Note:** Version bump only for package pubsweet-component-editoria-user-profile

<a name="0.1.0"></a>
# 0.1.0 (2019-10-14)


### Features

* **profile:** new user page profile ([6e0fc62](https://gitlab.coko.foundation/editoria/editoria/commit/6e0fc62))
