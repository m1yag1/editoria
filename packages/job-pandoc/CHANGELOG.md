# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pandoc-job@0.1.2...editoria-pandoc-job@0.1.3) (2020-03-03)




**Note:** Version bump only for package editoria-pandoc-job

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pandoc-job@0.1.1...editoria-pandoc-job@0.1.2) (2019-10-31)


### Bug Fixes

* **exporter:** icml fix standalone ([9bd270e](https://gitlab.coko.foundation/editoria/editoria/commit/9bd270e))




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-pandoc-job@0.1.0...editoria-pandoc-job@0.1.1) (2019-10-24)


### Bug Fixes

* **jobs:** change the paths ([ea47d2f](https://gitlab.coko.foundation/editoria/editoria/commit/ea47d2f))




<a name="0.1.0"></a>
# 0.1.0 (2019-10-23)


### Features

* **pandocjob:** create pandoc job ([c8eb472](https://gitlab.coko.foundation/editoria/editoria/commit/c8eb472))
