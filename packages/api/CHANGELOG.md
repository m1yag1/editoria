# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.5.1"></a>
## [0.5.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.5.0...editoria-api@0.5.1) (2020-03-03)




**Note:** Version bump only for package editoria-api

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.11...editoria-api@0.5.0) (2020-02-12)


### Features

* **api:** aws s3 integration ([088f24a](https://gitlab.coko.foundation/editoria/editoria/commit/088f24a))




<a name="0.4.11"></a>
## [0.4.11](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.10...editoria-api@0.4.11) (2020-01-09)


### Bug Fixes

* **api:** files remove fix ([ea8e5a8](https://gitlab.coko.foundation/editoria/editoria/commit/ea8e5a8))




<a name="0.4.10"></a>
## [0.4.10](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.9...editoria-api@0.4.10) (2020-01-09)


### Bug Fixes

* **api:** cleanup code, increase timeout ([8c05b0f](https://gitlab.coko.foundation/editoria/editoria/commit/8c05b0f))




<a name="0.4.9"></a>
## [0.4.9](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.8...editoria-api@0.4.9) (2019-12-17)




**Note:** Version bump only for package editoria-api

<a name="0.4.8"></a>
## [0.4.8](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.7...editoria-api@0.4.8) (2019-12-03)




**Note:** Version bump only for package editoria-api

<a name="0.4.7"></a>
## [0.4.7](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.6...editoria-api@0.4.7) (2019-11-06)


### Bug Fixes

* **exporter:** remove temp folders ([fbd8bbe](https://gitlab.coko.foundation/editoria/editoria/commit/fbd8bbe))




<a name="0.4.6"></a>
## [0.4.6](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.5...editoria-api@0.4.6) (2019-11-05)


### Bug Fixes

* **jobs:** fix in pdf and other bugs ([7bef376](https://gitlab.coko.foundation/editoria/editoria/commit/7bef376))




<a name="0.4.5"></a>
## [0.4.5](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.4...editoria-api@0.4.5) (2019-11-01)


### Bug Fixes

* **jobpdf:** correctionin of path ([e8b8021](https://gitlab.coko.foundation/editoria/editoria/commit/e8b8021))




<a name="0.4.4"></a>
## [0.4.4](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.3...editoria-api@0.4.4) (2019-10-30)


### Bug Fixes

* **wax:** new wax ([6c7a3e6](https://gitlab.coko.foundation/editoria/editoria/commit/6c7a3e6))




<a name="0.4.3"></a>
## [0.4.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.2...editoria-api@0.4.3) (2019-10-29)


### Bug Fixes

* **exporter:** typo in comments ([cc2a86d](https://gitlab.coko.foundation/editoria/editoria/commit/cc2a86d))




<a name="0.4.2"></a>
## [0.4.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.1...editoria-api@0.4.2) (2019-10-29)




**Note:** Version bump only for package editoria-api

<a name="0.4.1"></a>
## [0.4.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.4.0...editoria-api@0.4.1) (2019-10-25)


### Bug Fixes

* **jobs:** smaller payload ([505d53e](https://gitlab.coko.foundation/editoria/editoria/commit/505d53e))




<a name="0.4.0"></a>
# [0.4.0](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.3.4...editoria-api@0.4.0) (2019-10-23)


### Bug Fixes

* **language-tools:** take dynaimc port form env ([891be46](https://gitlab.coko.foundation/editoria/editoria/commit/891be46))


### Features

* **epubcheckjob:** pubsweet epubcheck job ([3896d18](https://gitlab.coko.foundation/editoria/editoria/commit/3896d18))
* **pandocjob:** create pandoc job ([c8eb472](https://gitlab.coko.foundation/editoria/editoria/commit/c8eb472))




<a name="0.3.4"></a>
## [0.3.4](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.3.3...editoria-api@0.3.4) (2019-10-16)


### Bug Fixes

* **exporter:** notes id and new window pdf ([e1c1bc9](https://gitlab.coko.foundation/editoria/editoria/commit/e1c1bc9))




<a name="0.3.3"></a>
## [0.3.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.3.2...editoria-api@0.3.3) (2019-10-15)


### Bug Fixes

* **exporter:** remove chapter end notes when none exist ([ffb6123](https://gitlab.coko.foundation/editoria/editoria/commit/ffb6123))




<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.3.1...editoria-api@0.3.2) (2019-10-15)


### Bug Fixes

* **exporter:** icml added and migration ([5f7efee](https://gitlab.coko.foundation/editoria/editoria/commit/5f7efee))




<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.3.0...editoria-api@0.3.1) (2019-10-14)


### Bug Fixes

* **export:** fix for navigation in wax and template notes ([8968fd9](https://gitlab.coko.foundation/editoria/editoria/commit/8968fd9))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.2.0...editoria-api@0.3.0) (2019-10-14)


### Bug Fixes

* **customtags:** fixes on subscription ([687bee7](https://gitlab.coko.foundation/editoria/editoria/commit/687bee7))
* **customtags:** subscription create fix ([e2165a4](https://gitlab.coko.foundation/editoria/editoria/commit/e2165a4))
* **navigation:** exclude notes and toc ([0bbfb6a](https://gitlab.coko.foundation/editoria/editoria/commit/0bbfb6a))
* **padedjs:** fix template texting to modals ([3f96227](https://gitlab.coko.foundation/editoria/editoria/commit/3f96227))
* **pagedStyle:** refresh paged iframe ([1d3dd4c](https://gitlab.coko.foundation/editoria/editoria/commit/1d3dd4c))


### Features

* **book builder:** init include in toc ([aafd09b](https://gitlab.coko.foundation/editoria/editoria/commit/aafd09b))
* **book builder:** running heads init ([df63d2f](https://gitlab.coko.foundation/editoria/editoria/commit/df63d2f))
* **customTag:** add subscription for custom tag update ([af4c7fd](https://gitlab.coko.foundation/editoria/editoria/commit/af4c7fd))
* **endnotes:** add footnotes component type ([72babd5](https://gitlab.coko.foundation/editoria/editoria/commit/72babd5))
* **export:** export overhaul ([c06ed43](https://gitlab.coko.foundation/editoria/editoria/commit/c06ed43))
* **exporter:** exporter modal done ([d825e39](https://gitlab.coko.foundation/editoria/editoria/commit/d825e39))
* **jobxsweet:** cleanup and seperate upload from convert ([e17e9aa](https://gitlab.coko.foundation/editoria/editoria/commit/e17e9aa))
* **jobxsweet:** integrate the new job xsweet ([a2bf999](https://gitlab.coko.foundation/editoria/editoria/commit/a2bf999))
* **jobxsweet:** replace ink with xsweet job ([0b5de67](https://gitlab.coko.foundation/editoria/editoria/commit/0b5de67))
* **profile:** new user page profile ([6e0fc62](https://gitlab.coko.foundation/editoria/editoria/commit/6e0fc62))
* **template:** save pagedjs to new template ([75e48c0](https://gitlab.coko.foundation/editoria/editoria/commit/75e48c0))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.6...editoria-api@0.2.0) (2019-08-01)


### Bug Fixes

* **applicationmanager:** fix models retrieve confgi form db ([e441cb1](https://gitlab.coko.foundation/editoria/editoria/commit/e441cb1))
* **applicationparameters:** rename config to applicationmanager props everywhere ([47b3538](https://gitlab.coko.foundation/editoria/editoria/commit/47b3538))
* **navigation:** fix navigation of editor link ([c569dc2](https://gitlab.coko.foundation/editoria/editoria/commit/c569dc2))
* **test:** make test work for editoria ([8868af9](https://gitlab.coko.foundation/editoria/editoria/commit/8868af9))


### Features

* **applicationmanager:** get live updates subscriptions ([632e471](https://gitlab.coko.foundation/editoria/editoria/commit/632e471))
* **applicationparameters:** get config from db create graphql query ([3a34792](https://gitlab.coko.foundation/editoria/editoria/commit/3a34792))
* **dynamicomponenttype:** add graphql schema query mutations table ([4e4081d](https://gitlab.coko.foundation/editoria/editoria/commit/4e4081d))
* **header:** easier navigation between bookcomponents ([9e675af](https://gitlab.coko.foundation/editoria/editoria/commit/9e675af))
* **tags:** add resolvers, graphql schema ([88bb4ca](https://gitlab.coko.foundation/editoria/editoria/commit/88bb4ca))
* **template manager:** init template manager ([cd51950](https://gitlab.coko.foundation/editoria/editoria/commit/cd51950))




<a name="0.1.6"></a>
## [0.1.6](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.5...editoria-api@0.1.6) (2019-05-28)




**Note:** Version bump only for package editoria-api

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.4...editoria-api@0.1.5) (2019-05-22)


### Bug Fixes

* **signup:** fix permission rule when new user created ([bea7622](https://gitlab.coko.foundation/editoria/editoria/commit/bea7622))




<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.3...editoria-api@0.1.4) (2019-04-25)


### Bug Fixes

* publication date fix ([88cbfea](https://gitlab.coko.foundation/editoria/editoria/commit/88cbfea))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.2...editoria-api@0.1.3) (2019-04-25)


### Bug Fixes

* more fixes ([f6028f5](https://gitlab.coko.foundation/editoria/editoria/commit/f6028f5))




<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.1...editoria-api@0.1.2) (2019-04-24)


### Bug Fixes

* **authorize:**  can fragment edit rule  foreach bookcomponent ([7a6c4c0](https://gitlab.coko.foundation/editoria/editoria/commit/7a6c4c0))
* **dashboard:** sorting fix for capital letters ([c09939f](https://gitlab.coko.foundation/editoria/editoria/commit/c09939f))
* **globalproduction:** add to team prodctionEditor non admin user ([c08e32f](https://gitlab.coko.foundation/editoria/editoria/commit/c08e32f))
* **reorder:** wrapper queries into one transaction ([10ab3e3](https://gitlab.coko.foundation/editoria/editoria/commit/10ab3e3))
* **trackchange:** get updates of the workflow and trackchange ([c0b0b3b](https://gitlab.coko.foundation/editoria/editoria/commit/c0b0b3b))
* more UI fixes ([78e14da](https://gitlab.coko.foundation/editoria/editoria/commit/78e14da))




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.0...editoria-api@0.1.1) (2019-04-12)


### Bug Fixes

* **editoria-api:** path fix ([3e62108](https://gitlab.coko.foundation/editoria/editoria/commit/3e62108))




<a name="0.1.0"></a>
# 0.1.0 (2019-04-12)


### Bug Fixes

* constants export and pubsweet server import ([399229a](https://gitlab.coko.foundation/editoria/editoria/commit/399229a))
* **authsome:** add athsome rules to resolvers ([1305074](https://gitlab.coko.foundation/editoria/editoria/commit/1305074))
* **authsome:** chagne query not get deleted ([d72044f](https://gitlab.coko.foundation/editoria/editoria/commit/d72044f))
* **authsome:** fix subscription for authosme updates ([1ce1fd9](https://gitlab.coko.foundation/editoria/editoria/commit/1ce1fd9))
* **authsome:** move team rules to bookbuilder ([0cab675](https://gitlab.coko.foundation/editoria/editoria/commit/0cab675))
* **globalteam:** migrate new tables for the globalteam page ([d8d0d37](https://gitlab.coko.foundation/editoria/editoria/commit/d8d0d37))
* **team:** clear unneeded imports ([77070b9](https://gitlab.coko.foundation/editoria/editoria/commit/77070b9))
* **teammanager:** teamanger add bug as search ([631c579](https://gitlab.coko.foundation/editoria/editoria/commit/631c579))


### Features

* **dashboard:** wire up refetch ui with graphql ([aa99cef](https://gitlab.coko.foundation/editoria/editoria/commit/aa99cef))
* bootstrap graphql ([8028771](https://gitlab.coko.foundation/editoria/editoria/commit/8028771))
* subscriptions in progress ([2d92222](https://gitlab.coko.foundation/editoria/editoria/commit/2d92222))
* **app:** Given name and surname now supported in Signup ([745ab2e](https://gitlab.coko.foundation/editoria/editoria/commit/745ab2e)), closes [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **authorize:** update rules in certain events fo the user ([89ee01e](https://gitlab.coko.foundation/editoria/editoria/commit/89ee01e))
* **authsome:** add subscriptions to get updates ([c042093](https://gitlab.coko.foundation/editoria/editoria/commit/c042093))
* **authsome:** move authorize to backend ([4e00def](https://gitlab.coko.foundation/editoria/editoria/commit/4e00def))
* **bookbuilder:** drag and drop ([bd21f36](https://gitlab.coko.foundation/editoria/editoria/commit/bd21f36))
* **dashboard:** archive books and user's full name support ([8bd0a25](https://gitlab.coko.foundation/editoria/editoria/commit/8bd0a25)), closes [#226](https://gitlab.coko.foundation/editoria/editoria/issues/226) [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **dashboard:** sorting books in dashboard ([d22c27e](https://gitlab.coko.foundation/editoria/editoria/commit/d22c27e))
* **globalteam:** migrate globalTeams to graphql ([2c6f870](https://gitlab.coko.foundation/editoria/editoria/commit/2c6f870))
* **teams:** migrate team to graphql ([cc161e9](https://gitlab.coko.foundation/editoria/editoria/commit/cc161e9))
